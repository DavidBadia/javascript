import React from "react";

import Foto from './Foto';
import Titulo from './Titulo';
import Interruptor from './Interruptor';
import Luz from './Luz';
import Copia from './Copia';

import Lista from './Lista';
import {CIUTATS} from './Datos';


export default () => (
  <>
    <Foto />
    <hr />
    <Titulo texto="Ejemplo nano-react" />
    <hr />
    <Interruptor texto="BOOLEAN" estadoInicial={true} />
    <hr />
    <Copia />
    <hr />
    <Lista items={CIUTATS} />
    <hr />
    <Luz estadoInicial={false} />
    <hr />

  </>
);
