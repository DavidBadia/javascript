import React from "react";
import Palabra from './Componentes/Palabra';
import Lista from './Componentes/Lista';

import { Container, Row, Col } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';
import './Componentes/App.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      palabras: []
    }
    this.nuevaPalabra = this.nuevaPalabra.bind(this);
    this.borraLlista = this.borraLlista.bind(this);
  }
  nuevaPalabra(palabra) {
    return this.setState({ palabras: [...this.state.palabras, palabra] });
  }
  borraLlista() {
    return this.setState({ palabras: [] });
  }
  render() {
    return (
      <Container className="mt-5">
        <Row>
          <Col xs="12">
            <h1>Evaluació David Badia</h1>
            <Palabra nuevaPalabra={this.nuevaPalabra} borraLlista={this.borraLlista} />
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Lista palabras={this.state.palabras} />
          </Col>
        </Row>
      </Container>
    );
  }
}