import React from "react";
import { Button, Form, FormGroup, Input } from 'reactstrap';

import './App.css';

export default class Palabra extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            palabra: ""
        }
        this.obtenerDatos = this.obtenerDatos.bind(this);
        this.envio = this.envio.bind(this);
    }
    obtenerDatos(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    envio(e) {
        e.preventDefault();
        this.props.nuevaPalabra(this.state.palabra);
    }
    
    render() {
        return (
            <div className="formulari">
                <Form inline onSubmit={this.envio}>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Input type="text" name="palabra" id="palabra" placeholder="Introdueix text" onChange={this.obtenerDatos} />
                    </FormGroup>
                    <Button className="mr-2" color="primary" type="submit">Afegeix</Button>
                    <Button color="danger" onClick={this.props.borraLlista}>Esborra</Button>
                </Form>
            </div>
        );
    }
}