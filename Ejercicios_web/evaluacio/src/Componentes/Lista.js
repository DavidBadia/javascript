import React from "react";

export default class Lista extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let listaPalabras = this.props.palabras;
        let lista = listaPalabras.map(el => <li key={el}>{el}</li>);
        return (
            <>
                <ul>
                    {lista}
                </ul>
            </>
        );
    }
}