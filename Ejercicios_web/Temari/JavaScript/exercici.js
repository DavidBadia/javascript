//Le pasamos a una función dos números y nos devuelve el mayor de ellos
function mayor(a, b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}
console.log(mayor(2, 4));

//muestra en pantalla datos referidos al número recibido
var i = 1240;

if (i % 2 === 0) {
    console.log("1240 es par");
}
if (i % 3 !== 0) {
    console.log("NO es divisible por 3");
}
if (i % 5 === 0) {
    console.log("SI es divisible por 5");
}
if (i % 7 !== 0) {
    console.log("NO es divisible por 7");
}

//Recibe un array de valores numéricos, devuelve la suma de todos ellos 
var sumaValores = [1, 3, 9, 2, 3];
var total = 0
for (var i = 0; i < sumaValores.length; i++) {
    total = total + sumaValores[i]
}
console.log(total);

//Devuelve el producto factorial del número recibido
var numero = 10;
var resultado = 1;
for (var i = 1; i <= numero; i++) {
    resultado *= i;
}

console.log(resultado);

//Un número es primo si sólo es divisible por 1 y por sí mismo
function primo(num) {
    var esPrimo = true;
    for (var i = num - 1; i > 1; i--) {
        if (num % i === 0) {
            esPrimo = false;
            break;
        }
    }
    if (esPrimo) {
        return ("El numero " + num + " es primo.");
    }
    else {
        return ("El numero " + num + " no es primo.");
    }
}

//En la serie de Fibonacci cada número es la suma de los dos anteriores.
function fibonacci(numero) {
    var primero = 0;
    var segundo = 1;
    var tercero = 1;
    var cuarto = 1;
    console.log(primero, segundo);
    for (i = 2; i < numero; i++) {
        tercero = primero + segundo;
        console.log(tercero);
        cuarto = cuarto + tercero;
        primero = segundo;
        segundo = tercero;
    }
}

//convierte primera letra en mayúscula, resto en minúscula
function capitaliza(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

//Longitud de texto, Letra de una palabra
function letras(palabra) {
    var vocales = 0;
    var consonantes = 0;

    for (var i = 0; i < palabra.length; i++) {
        letra = palabra[i];
        if (letra == "a" || letra == "e" || letra == "i" || letra == "o" || letra == "u") {
            vocales++;
            console.log(letra + " es vocal");
        } else {
            consonantes++;
            console.log(letra + " es consonante");
        }
        var total = vocales + consonantes;

    }
    console.log(palabra + " tiene " + total + " letras")
    console.log("Vocales: " + vocales)
    console.log("Consonantes: " + consonantes)
}

//Muestra el nombre del dia de la semana.
var dias = ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"];
function fechaHoy() {
    var hoy = new Date();
    var diasem = dias[hoy.getDate()];
    console.log(` Hoy es ${diasem}`);
}

//Muestra los dias que faltan para Navidad.
Navidad = new Date(2019, 11, 24);
Hoy = new Date(2019, 10, 28);
dias(Navidad, Hoy);

function dias(fecha1, fecha2) {
    var ms1 = fecha1.getTime();
    var ms2 = fecha2.getTime();
    var diferencia = ms1 - ms2;
    var diasRestantes = diferencia / 1000 / 60 / 60 / 24;
    console.log("Faltan " + diasRestantes + " dias para Navidad");
}

//aleatorio 
function numAleatorio() {
    var random = parseInt(Math.random() * 11);
    console.log(random);
    var num;
    while (num !== random) {
        num = prompt("Elige un numero del 1 al 10");
        if (num === random) {
            alert("Acertastes");
        }
        else if (num === 0) {
        }
        else if (num < random) {
            alert("Demasiado bajo, vuelve a intentar...");
        }
        else if (num > random) {
            alert("Demasiado alto, vuelve a intentar...");
        }
    }
}