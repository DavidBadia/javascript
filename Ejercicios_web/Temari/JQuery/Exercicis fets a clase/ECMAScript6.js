class Figura {
    constructor(x,y){
      this.x = x;
      this.y = y;
    }
    posicion(){
      console.log(`(${this.x}, ${this.y})`);
    }
  }
  
  class Rectangulo extends Figura {
    constructor(x,y,lado1,lado2){
      super(x,y);
      this.lado1 = lado1;
      this.lado2 = lado2;
    }
    area(){
      let sup = this.lado1 * this.lado2;
      console.log(`area = ${sup}`);
    }
  }
  
  
  class Triangulo extends Figura {
    constructor(x,y,base,altura){
     super();
      this.base = base;
      this.altura = altura;
    }
    area(){
      let sup = this.base * this.altura / 2;
      console.log(`area = ${sup}`);
    }
  }
  
  class Cuadrado extends Rectangulo {
    constructor(x,y,lado){
      super(x,y, lado, lado);
    }
  }
  
  
  let rect=new Rectangulo(3,2,10,20);
  rect.posicion();
  rect.area();
  
  let tri=new Triangulo(3,2,10,20);
  tri.posicion();
  tri.area();
  
  let cuadrado=new Cuadrado(3,2,5);
  cuadrado.posicion();
  cuadrado.area();
  
  
  /*
  class Triangulo extends Figura {
    constructor(x,y,base,altura){
      
    }
  }
  
  class Cuadrado extends Rectangulo {
    constructor(x,y,lado){
      
    }
  }
  
  let rect=new Rectangulo(0,0,10,20);
  console.log(rect.area());
  
  let tri=new Triangulo(0,0,10,8);
  console.log(tri.area());
  
  let cua=new Cuadrado(0,0,10);
  console.log(cua.area());
  */