import React, { Component } from 'react';

import Contador from './components/Contador'
import Calculadora from './components/Calculadora'

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Contador valorInicial="5" />
        <Calculadora />
      </React.Fragment>
    );
  }
}

export default App;