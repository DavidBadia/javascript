import React from 'react';
import './css/Calculadora.css';

export default class Teclado extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            display: "0",
            valorAnterior: 0,
            operacionAnterior: "",
            vaciarDisplay: true
        }
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar(caracter) {
        if (caracter == "C") {
            this.setState({
                display: 0
            });
        } else if (caracter === "+") {
            this.setState({
                valorAnterior: this.state.display,
                operacionAnterior: "+",
                display: ""
            })
        } else if (caracter === "-") {
            this.setState({
                valorAnterior: this.state.display,
                operacionAnterior: "-",
                display: ""
            })
        } else if (caracter === "=") {
            let resultado = 0;

            if (this.state.operacionAnterior === "+") {
                resultado = this.state.display * 1 + this.state.valorAnterior * 1;
            } else {
                resultado = this.state.valorAnterior * 1 - this.state.display * 1;

            } this.state.valorAnterior = "0";
            this.setState({
                display: resultado,
                vaciarDisplay: true
            })
        } else {
            if (this.state.vaciarDisplay === true) {
                this.setState({
                    display: caracter,
                    vaciarDisplay: false
                });
            } else {
                this.setState({
                    display: "" + this.state.display + caracter
                });
            }

        }


    }
    render() {
        return (
            <React.Fragment>
                <div className="calculadora">
                    <h3 className="pantalla">{this.state.display}</h3>
                    <div className="Teclado">
                        <div>
                            <button className="calcBoton" onClick={() => this.pulsar(1)} >1</button>
                            <button className="calcBoton" onClick={() => this.pulsar(2)} >2</button>
                            <button className="calcBoton" onClick={() => this.pulsar(3)} >3</button>
                            <button className="calcBoton" onClick={() => this.pulsar("/")} >/</button>
                        </div>
                        <div>
                            <button className="calcBoton" onClick={() => this.pulsar(4)} >4</button>
                            <button className="calcBoton" onClick={() => this.pulsar(5)} >5</button>
                            <button className="calcBoton" onClick={() => this.pulsar(6)} >6</button>
                            <button className="calcBoton" onClick={() => this.pulsar("*")} >*</button>
                        </div>
                        <div>
                            <button className="calcBoton" onClick={() => this.pulsar(7)} >7</button>
                            <button className="calcBoton" onClick={() => this.pulsar(8)} >8</button>
                            <button className="calcBoton" onClick={() => this.pulsar(9)} >9</button>
                            <button className="calcBoton" onClick={() => this.pulsar("-")} >-</button>
                        </div>
                        <div>
                            <button className="calcBoton" onClick={() => this.pulsar(0)} >0</button>
                            <button className="calcBoton" onClick={() => this.pulsar("C")} >C</button>
                            <button className="calcBoton" onClick={() => this.pulsar("=")} >=</button>
                            <button className="calcBoton" onClick={() => this.pulsar("+")} >+</button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }

}