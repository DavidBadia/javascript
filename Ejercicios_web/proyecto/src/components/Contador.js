import React from 'react';

import Dato from './Dato';
import Boton from './Boton';


class Contador extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            display: this.props.valorInicial * 1
        };

        this.displayMas = this.displayMas.bind(this);
        this.displayMenos = this.displayMenos.bind(this);

    }

    displayMenos() {
        // NO HACER ESTO: this.state.display--;
        if (this.state.display > 0) {
            this.setState({
                display: this.state.display - 1
            });
        }
    }

    displayMas() {
        // NO HACER ESTO: this.state.display++;
        if (this.state.display < 8) {
            this.setState({
                display: this.state.display + 1
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <Boton valor={this.state.display} clicado={this.displayMenos} texto="Menos" />
                <Dato valor={this.state.display} />
                <Dato valor={this.state.display + 1} />
                <Dato valor={this.state.display + 2} />
                <Boton valor={this.state.display} clicado={this.displayMas} texto="Mas" />
            </React.Fragment>
        );
    }
}


export default Contador;