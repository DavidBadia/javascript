import React, { Component } from 'react';


class Finger extends Component {

    constructor(props) {
        super(props);
        this.state = { position: 0 }
        this.clicar = this.clicar.bind(this);
    }
    clicar() {

        if (this.state.position < 3) {
            this.setState({ position: this.state.position + 1 });
        }
        else {
            (this.setState({ position: 0 }));
        }

    }

    render() {
        let clases = ["far fa-hand-point-up fa-7x", "far fa-hand-point-right fa-7x", "far fa-hand-point-down fa-7x", "far fa-hand-point-left fa-7x"];
        let posiciones = clases[this.state.position];
        return (<div onClick={this.clicar} className={posiciones}></div>);

    }

}
export default Finger;


