import React, { Component } from 'react';
import Bici from "./img/bici.jpg";
import Coche from "./img/coche.jpg";
import Moto from "./img/moto.jpeg";
import Bus from "./img/bus.jpg";


export default class Fotos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foto: "bici",
            img: Bici
        }
        this.selecciona = this.selecciona.bind(this);


    }


    selecciona(evento) {
        let valorSeleccionado = evento.target.value;
        this.setState({ foto: valorSeleccionado });
        if (valorSeleccionado === "bici") {
            this.setState({ img: Bici });
        }
        if (valorSeleccionado === "coche") {
            this.setState({ img: Coche });
        }
        if (valorSeleccionado === "moto") {
            this.setState({ img: Moto });
        }
        if (valorSeleccionado === "bus") {
            this.setState({ img: Bus });
        }
    }
    render() {
        return (
            <>

                <select onChange={this.selecciona}>
                    <option value="coche">Coche</option>
                    <option value="moto">Moto</option>
                    <option value="bici">Bici</option>
                    <option value="bus">Bus</option>
                </select>
                <img widht="100px" height="100px" src={this.state.img} />



            </>
        )
    }
}
