import React from 'react';



export default class Thumbs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            on: this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar() {
        this.setState({
            on: !this.state.on
        })
    }

    render() {
        let claseIcono = (this.state.on) ? "far fa-thumbs-down fa-7x" : "far fa-thumbs-up fa-7x";
        let claseDiv = (this.state.on) ? "off" : "on";
        return (
            <div onClick={this.pulsar} className={claseDiv}>
                <i className={claseIcono} />
                <span>{this.props.texto}</span>
            </div>
        );
    }

}


