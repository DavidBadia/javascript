import React from 'react';
import { Container, Row, Col, Table } from 'reactstrap';

const icones = {
    '01d': { img: "http://openweathermap.org/img/w/01d.png", text: "Cel clar" },
    '02d': { img: "http://openweathermap.org/img/w/02d.png", text: "Pocs núvols" },
    '03d': { img: "http://openweathermap.org/img/w/03d.png", text: "Núvols dispersos" },
    '04d': { img: "http://openweathermap.org/img/w/04d.png", text: "Núvols trencats" },
    '09d': { img: "http://openweathermap.org/img/w/09d.png", text: "Ruixats" },
    '10d': { img: "http://openweathermap.org/img/w/10d.png", text: "Pluja" },
    '11d': { img: "http://openweathermap.org/img/w/11d.png", text: "Tempesta" },
    '13d': { img: "http://openweathermap.org/img/w/13d.png", text: "Neu" },
    '50d': { img: "http://openweathermap.org/img/w/50d.png", text: "Boira" },
    '01n': { img: "http://openweathermap.org/img/w/01n.png", text: "Cel clar" },
    '02n': { img: "http://openweathermap.org/img/w/02n.png", text: "Pocs núvols" },
    '03n': { img: "http://openweathermap.org/img/w/03n.png", text: "Núvols dispersos" },
    '04n': { img: "http://openweathermap.org/img/w/04n.png", text: "Núvols trencats" },
    '09n': { img: "http://openweathermap.org/img/w/09n.png", text: "Ruixats" },
    '10n': { img: "http://openweathermap.org/img/w/10n.png", text: "Pluja" },
    '11n': { img: "http://openweathermap.org/img/w/11n.png", text: "Tempesta" },
    '13n': { img: "http://openweathermap.org/img/w/13n.png", text: "Neu" },
    '50n': { img: "http://openweathermap.org/img/w/50n.png", text: "Boira" }
};


export default class Previsio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            previsions: {}

        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();

    }

    getPrevisio() {
        //consultar formato: https://openweathermap.org/forecast5#name5
        //http://api.openweathermap.org/data/2.5/forecast?q=barcelona,es&APPID=84dbcf8c3480649bce9d4bb58da44b4e&units=metric
        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.analitzaDades(data.list))
            .then(previsions => this.setState({ previsions }))
            .catch(error => console.log(error));
    }

    analitzaDades(llista) {
        let previsions = {};

        const primer = llista[0];
        let dataActual = new Date(primer.dt * 1000);
        let dia = dataActual.getDate();
        previsions[dia] = {
            max: primer.main.temp_max,
            min: primer.main.temp_min,
            icona: primer.weather[0].icon,
            data: dataActual
        };

        llista.forEach(item => {
            dataActual = new Date(item.dt * 1000);
            if (dataActual.getDate() === dia) {
                if (previsions[dia].max < item.main.temp_max) {
                    previsions[dia].max = item.main.temp_max;
                }
                if (previsions[dia].min > item.main.temp_min) {
                    previsions[dia].min = item.main.temp_min;
                }
                if (previsions[dia].icona.substring(0, 2) < item.weather[0].icon.substring(0, 2)) {
                    previsions[dia].icona = item.weather[0].icon;
                }

            } else {
                dia = dataActual.getDate();
                previsions[dia] = {
                    max: item.main.temp_max,
                    min: item.main.temp_min,
                    icona: item.weather[0].icon,
                    data: dataActual
                };
            }
        });

        return previsions;


    }


    render() {

        if (this.state.previsions.length === 0) {
            return <h1>Cargando datos...</h1>
        }

        let files = [];

        for (var property in this.state.previsions) {
            if (this.state.previsions.hasOwnProperty(property)) {
                let dia = this.state.previsions[property];
                files.push(<tr key={property}>
                    <td>{property}</td>
                    <td>{dia.max}</td>
                    <td>{dia.min}</td>
                    <td><img src={icones[dia.icona].img} />{icones[dia.icona].text}</td><td>{dia.data.toLocaleDateString("es-ES")}</td>
                </tr>);
            }
        }

        return (
            <>
                <Container>
                    <Row>
                        <Col>
                            <Table>
                                <thead>
                                    <tr><th>Dia</th><th>Maxima</th><th>Minima</th><th>Icona</th><th>Data</th></tr>
                                </thead>
                                <tbody>
                                    {files}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>

            </>
        );
    }

}