import React from "react";
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';


import Thumbs from './Thumbs';
import Finger from './Thumbs_modificado';
import Tricolor from './Tricolor';
import Fotos from './Fotos';
import Comarcas from './Comarcas';
import { CIUTATS_CAT_20K } from './Datos';
import Previsio from './Previsio';
import Gente from './Gente';


export default () => (
  <BrowserRouter>
    <Container>
      <Row>
        <Col>
          <ul>
            <li><Link to="/Thumbs">Ejercicio 1: Thumbs</Link></li>
            <li><Link to="/Finger">Ejercicio 1B: Thumbs Modificado</Link></li>
            <li><Link to="/Tricolor">Ejercicio 2: Tricolor</Link></li>
            <li><Link to="/Fotos">Ejercicio 3: Fotos</Link></li>
            <li><Link to="/Comarcas">Ejercicio 4: Comarcas</Link></li>
            <li><Link to="/Previsio">Ejercicio 5: Previsio</Link></li>
            <li><Link to="/Gente">Ejercicio 6: Gente</Link></li>
          </ul>
        </Col>
      </Row>
      <Switch>
        <Route path="/Thumbs" component={Thumbs} />
        <Route path="/Finger" component={Finger} />
        <Route path="/Tricolor" component={Tricolor} />
        <Route path="/Fotos" component={Fotos} />
        <Route path="/Comarcas" render={() => <Comarcas items={CIUTATS_CAT_20K} />} />
        <Route path="/Previsio" component={Previsio} />
        <Route path="/Gente" component={Gente} />
      </Switch>
    </Container>
  </BrowserRouter>

);


