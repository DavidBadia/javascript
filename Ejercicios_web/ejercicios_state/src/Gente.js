import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';


const Gente = (props) => {
    return (
        <div>
            <Card>
                <CardImg top width="100%" src="" alt="Foto" />
                <CardBody>
                    <CardTitle>Card title</CardTitle>
                    <CardSubtitle>Card subtitle</CardSubtitle>
                    <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                    <Button>OK!</Button>
                </CardBody>
            </Card>
        </div>

    );

};

export default Gente;