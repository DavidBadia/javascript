import React, { Component } from 'react';
import './css/tricolor.css';

class Tricolor extends Component {

    constructor(props) {
        super(props);
        this.state = { color: 0 }
        this.clicar = this.clicar.bind(this);
    }
    clicar() {

        if (this.state.color < 3) {
            this.setState({ color: this.state.color + 1 });
        }
        else {
            (this.setState({ color: 0 }));
        }
    }

    render() {
        let clases = ["circulo", "circulo rojo", "circulo verde", "circulo azul"];
        let triClase = clases[this.state.color];
        return (<div onClick={this.clicar} className={triClase}></div>);
    }

}
export default Tricolor;



