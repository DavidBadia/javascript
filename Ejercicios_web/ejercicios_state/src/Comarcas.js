import React from 'react';



class Comarcas extends React.Component {
    constructor(props) {
        super(props);

        let comarcasRepetidas = this.props.items.map(elemento => elemento.comarca);
        let comarcasUnicas = [];
        for (let i = 0; i < comarcasRepetidas.length; i++) {
            if (comarcasUnicas.indexOf(comarcasRepetidas[i]) === -1) {
                comarcasUnicas.push(comarcasRepetidas[i]);
            }
        }
        comarcasUnicas.sort();

        this.state = {
            comarcasUnicas,
            comarcaActual: comarcasUnicas[0]
        }

        this.cambiaComarca = this.cambiaComarca.bind(this);
    }


    cambiaComarca(evento) {
        // let nuevaComarca = evento.target.value;
        this.setState({ comarcaActual: evento.target.value });

    }



    render() {
        let opcionsComarcasUnicas = this.state.comarcasUnicas.map(el => <option key={el} value={el}>{el}</option>);

        let opcionsMunicipis = this.props.items
            .filter(elemento => elemento.comarca === this.state.comarcaActual)
            .map(elemento => elemento.municipi)
            .map(el => <option key={el} value={el}>{el}</option>);


        return (
            <>
                <select onChange={this.cambiaComarca} >
                    {opcionsComarcasUnicas}
                </select>
                <br />
                <select  >
                    {opcionsMunicipis}
                </select>


                <h1>{this.state.comarcaActual}</h1>
            </>
        );
    }
}

export default Comarcas;