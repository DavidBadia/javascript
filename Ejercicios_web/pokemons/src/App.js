import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';

import ListaPokemons from './Componentes/ListaPokemons';
import DetallPokemon from './Componentes/DetallPokemon'
import Pokemon from './Componentes/Pokemon'
import logo from './Componentes/img/logo.png';
import P404 from './Componentes/P404';

import 'bootstrap/dist/css/bootstrap.min.css'
import './componentes/estilos.css'

//xmysql -h localhost -u root -d pokemons
export default class App extends React.Component {

  render() {



    return (
      <BrowserRouter>
        <Container>
          <div>
            <img className="logo" src={logo} />
            <h2>La mejor lista del Pokemundo!</h2>
          </div>

          <Row>

            <Col>
              <Switch>
                <Route exact path='/pokemons' component={ListaPokemons} />
                <Route path='/pokemons/:id' component={DetallPokemon} />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}




