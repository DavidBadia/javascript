import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import Pokemon from './Pokemon';
import './estilos.css'


const FotoPokemon = (props) => <img width="400px" src={"https://img.pokemondb.net/artwork/large/" + props.nombre.toLowerCase() + ".jpg"} />;

class DetallPokemon extends Component {
    constructor(props) {
        super(props);

        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }

        this.state = {
            tornar: false,

        }

        this.carregaRegistre = this.carregaRegistre.bind(this);
        this.torna = this.torna.bind(this);

        if (id > 0) {
            this.carregaRegistre(id);
        }
    }

    torna() {
        this.setState({ tornar: true });
    }
    //carreguem registre a DetallPokemonr
    carregaRegistre(id) {
        const fetchURL = "http://localhost:3000/api/pokemons/" + id;
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState(data[0]))
            .catch(err => console.log(err));
    }



    render() {


        if (this.state.nombre === undefined) {
            return <></>;
        }

        if (this.state.tornar === true) {
            return <Redirect to={'/pokemons'} />
        }



        return (
            <div className="text-center" >
                <h1 className="titol-vista">{"Has elegido a " + this.state.nombre}!</h1>
                <FotoPokemon nombre={this.state.nombre} />
                <br />
                <br />
                <Button color="primary" onClick={this.torna}>Volver</Button>

            </div>
        );
    }
}






export default DetallPokemon;