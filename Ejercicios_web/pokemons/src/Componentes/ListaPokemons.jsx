import React from "react";

import { Button, Table } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

import './estilos.css'

const IconoPokemon = (props) => {
    let imagen = "";
    if (props.girado) {
        imagen = <img src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/" + props.id + ".png"} />;
    } else {
        imagen = <img src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + props.id + ".png"} />;
    }
    return imagen;
};

const IconoPokemonShinny = (props) => {
    let imagen = "";
    if (props.girado) {
        imagen = <img src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/" + props.id + ".png"} />;
    } else {
        imagen = <img src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" + props.id + ".png"} />;
    }
    return imagen;
};

const FotoPokemon = (props) => <img width="100px" src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + props.id + ".png"} />;


export default class ListaPokemons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
            girado: false
        }
        this.getData = this.getData.bind(this);
        this.girar = this.girar.bind(this);
        this.girarS = this.girarS.bind(this);
        this.getData();
    }

    girar() {
        this.setState({ girado: !this.state.girado });
    }

    girarS() {
        this.setState({ girado: !this.state.girado });
    }

    getData() {
        const fetchURL = "http://localhost:3000/api/pokemons?_size=100";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ pokemons: data }))
            .catch(err => console.log(err));
    }


    render() {


        if (this.state.pokemons.length === 0) {
            return <>Loading...</>;
        }

        let filas = this.state.pokemons.map(item => {
            return (

                <tr key={item.id}>
                    <td >{item.id}</td>
                    <td ><Button color="Link" onClick={this.girar}><IconoPokemon girado={this.state.girado} id={item.id} /></Button></td>
                    <td ><Button color="Link" onClick={this.girarS}><IconoPokemonShinny girado={this.state.girado} id={item.id} /></Button></td>
                    <td>{item.nombre}</td>
                    <td>{item.caracter}</td>
                    <td><Link to={`/pokemons/${item.id}`} className="btn btn-primary">Detall</Link></td>

                </tr>

            );
        })

        return (
            <>
                <h1 className="titol-vista">Pokemons</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Normal</th>
                            <th>Shinny</th>
                            <th>Nombre</th>
                            <th>Caracter</th>
                        </tr>

                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>

            </>
        );
    }

}
