import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from "react-router-dom";
import { Container } from 'reactstrap';


import Home from './Componentes/Home';
import Ruleta from './Componentes/Ruleta';


import 'bootstrap/dist/css/bootstrap.min.css'




class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Container>


          <Route exact path="/" component={Home} />
          <Route path="/ruleta" component={Ruleta} />
        </Container>
      </BrowserRouter>

    );
  }
}

export default App;