import React, { Component } from 'react';
import {
    Carousel,
    CarouselItem,
    CarouselCaption,
    CarouselControl
} from 'reactstrap';
import './Carrousel.css';

const items = [
    {
        src: 'https://picsum.photos/1200/400',
        altText: 'Slide 1',
        header: 'YA TE DESGRACIATIS YOU STUPID FAT ASS!',
        caption: '-Albert Einstein.-',
    },
    {
        src: 'https://picsum.photos/1201/400',
        altText: 'Slide 2',
        header: 'Y SI LE DOY A ESTE BOTON?',
        caption: '-Operario de Chernobyl.-',
    },
    {
        src: 'https://picsum.photos/1202/400',
        altText: 'Slide 3',
        header: 'NO ES POR NO IR, PERO IR PA NA ES TONTERIA.',
        caption: '-Neil Armstrong.-',
    },
    {
        src: 'https://picsum.photos/1203/400',
        altText: 'Slide 4',
        header: 'MEJOR DEJO DE BEBER ANTES DE PERDER LA CABEZA',
        caption: '-JF Kennedy.-',
    },
    {
        src: 'https://picsum.photos/1204/400',
        altText: 'Slide 5',
        header: 'LO VEO Y NO LO CREO',
        caption: '-Steve Wonder.-',
    },
    {
        src: '/img/mariano.jpg',
        altText: 'Slide 6',
        header: 'SOMOS SENTIMIENTOS Y TENEMOS SERES HUMANOS',
        caption: '-M. Rajoy.-',
    },
    {
        src: 'https://picsum.photos/1206/400',
        altText: 'Slide 7',
        header: 'SUJETAME EL PORRO',
        caption: '-Elon Musk.-',
    },
];

class Mini extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    render() {
        const { activeIndex } = this.state;

        const slides = items.map((item) => {
            return (
                <CarouselItem className="mini"
                    tag="div"
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >
                    <img src={item.src} alt={item.altText} />
                    <CarouselCaption
                        className="texto-carrousel"
                        captionText={item.caption}
                        captionHeader={item.header} />
                </CarouselItem>
            );
        });

        return (
            <Carousel
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
            >
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
            </Carousel>
        );
    }
}


export default Mini;