import React, { Component } from 'react';
import {
    Carousel,
    CarouselItem,
    CarouselCaption,
    CarouselControl
} from 'reactstrap';
import './Carrousel.css';

const items = [
    {
        src: '/img/fullhd.png',
        altText: 'fullhd',
    },
    {
        src: '/img/react.png',
        altText: 'react',
    },
    {
        src: '/img/dia.png',
        altText: 'dia',
    },
    {
        src: '/img/solan.png',
        altText: 'solan',
    },
    {
        src: '/img/bitcoin.png',
        altText: 'bitcoin',
    },
    {
        src: '/img/platano.png',
        altText: 'platano',
    },
];

class Lateral extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    render() {
        const { activeIndex } = this.state;

        const slides = items.map((item) => {
            return (
                <CarouselItem className="lateral"
                    tag="div"
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >
                    <img src={item.src} alt={item.altText} />
                    <CarouselCaption
                        className="texto-carrousel"
                        captionText={item.caption}
                        captionHeader={item.header} />
                </CarouselItem>
            );
        });

        return (
            <Carousel
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
            >
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
            </Carousel>
        );
    }
}

export default Lateral;