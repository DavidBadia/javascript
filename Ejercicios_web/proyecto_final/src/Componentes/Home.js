import React, { Component } from 'react';
import Carrousel from './Carrousel';
import Mini from './MiniCarrousel';
import Lateral from './LateralCarrousel';
import './Carrousel.css';

class Home extends Component {
    render() {
        return (
            <div>
                <div className="principal">
                    <Carrousel />
                    <Mini />
                </div>
                <div className="secondary">
                    <Lateral />
                </div>
            </div>
        );
    }
}
export default Home;

