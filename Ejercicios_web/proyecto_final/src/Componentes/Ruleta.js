import React from 'react';
import ImagenRuleta from './img/ruleta.png'
import Flechita from './img/flechita.png'
import Coin from './img/coin.png'

import './Ruleta.css';


class Ruleta extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({ style: {}, ultimaTirada: 0, claseFlechita: '' })
        this.girarRuleta = this.girarRuleta.bind(this);
        this.flechitaDejarTemblar = this.flechitaDejarTemblar.bind(this);

    }
    girarRuleta(grados) {
        const tirada = Math.floor(this.state.ultimaTirada + grados + 2160);
        const style = { transform: `rotate(${tirada}deg)`, transition: "transform 3s ease-out" };
        this.setState({ style, ultimaTirada: tirada, claseFlechita: "temblando" });
        console.log(grados);
        this.flechitaDejarTemblar();


    }
    flechitaDejarTemblar() {
        setTimeout(() => {
            this.setState({ claseFlechita: '' });
        }, 3000);
    }

    render() {
        return (
            <React.Fragment>
                <button onClick={() => this.girarRuleta(Math.floor(Math.random() * 360))}>Girar</button>
                <div className="componenteRuleta">
                    <img className={`flechita ${this.state.claseFlechita}`} src={Flechita} alt="flechita"></img>
                    <img className="ruleta" style={this.state.style} src={ImagenRuleta} width="500" alt="ruleta"></img>
                    <img className="fondo" src={Coin} width="530" alt="fondo"></img>
                    <img className="coin" style={this.state.style} src={Coin} width="20" alt="coin"></img>

                </div>

            </React.Fragment>
        )
    }
}

export default Ruleta;