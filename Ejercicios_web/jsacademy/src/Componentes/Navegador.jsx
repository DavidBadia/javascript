import React from 'react';
import { Nav, NavItem, Container } from 'reactstrap';
import { Link } from "react-router-dom";

export default class Navegador extends React.Component {
    render() {
        return (
            <Container>
                <h1>JS-Academy</h1>
                <Nav>
                    <NavItem>
                        <Link to="/" className="nav-link">Home</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/alumnos" className="nav-link">Alumnos</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/cursos" className="nav-link">Cursos</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/nuevoalumno" className="nav-link">Nuevo Alumno</Link>
                    </NavItem>
                    <NavItem>
                        <Link to="/nuevocurso" className="nav-link">Nuevo Curso</Link>
                    </NavItem>
                </Nav>
            </Container>


        );
    }
}