import React, { Component } from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';

import { Redirect } from 'react-router-dom';



class NuevoAlumno extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            email: '',
            edad: '',
            genero: '',
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }


    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }


    submit(e) {
        this.props.guardaContacto({
            nombre: this.state.nombre,
            email: this.state.email,
            id: 0
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/alumnos' />
        }

        return (

            <Form onSubmit={this.submit}>
                <Row form>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="nombreInput">Nombre Completo</Label>
                            <Input type="text"
                                name="nombre"
                                id="nombreInput"
                                value={this.state.nombre}
                                onChange={this.cambioInput}
                                placeholder="Introducir Nombre y Apellidos" />

                        </FormGroup>
                    </Col>
                    <Col md={2}>
                        <FormGroup>
                            <Label for="edadInput">Edad</Label>
                            <Input type="text"
                                name="edad"
                                id="nombreInput"
                                value={this.state.edad}
                                onChange={this.cambioInput}
                                placeholder="Introducir Edad" />

                        </FormGroup>
                    </Col>

                </Row>
                <Row form>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput}
                                placeholder="Introducir E-mail" />

                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Label for="generoInput">Genero</Label>
                            <Input type="select"
                                name="genero"
                                id="generoInput"
                                value={this.state.genero}
                                onChange={this.cambioInput}>


                                <option>Masculino</option>
                                <option>Femenino</option>
                                <option>Undefinido</option>
                                <option>Helicoptero Apache</option>

                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row form>



                </Row>

                <Button color="primary">Guardar</Button>
            </Form >
        );

    }

}





export default NuevoAlumno;