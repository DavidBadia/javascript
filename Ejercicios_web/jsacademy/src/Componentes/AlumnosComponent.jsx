import React, { Component } from 'react';
import { Table } from 'reactstrap';


class Alumnos extends Component {
    constructor(props) {
        super(props);

    }

    render() {



        let filas = this.props.contactos.sort((a, b) => a.id - b.id).map(contacto => {
            return (
                <tr key={contacto.id}>
                    <td>{contacto.id}</td>
                    <td>{contacto.nombre}</td>
                    <td>{contacto.email}</td>
                    <td>{contacto.edad}</td>
                    <td>{contacto.genero}</td>

                </tr>
            );
        })


        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        );
    }


}

export default Alumnos;


