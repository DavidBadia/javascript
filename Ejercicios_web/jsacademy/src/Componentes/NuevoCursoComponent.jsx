import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Redirect } from 'react-router-dom';


export default class NuevoAlumno extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            especialidad: '',
            volver: false
        }
        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submit(e) {
        this.props.guardaNombre({
            nombre: this.state.nombre,
            especialidad: this.state.especialidad,
            id: 0
        });
        e.preventDefault();
        this.setState({ volver: true });
    }


    render() {
        if (this.state.volver === true) {
            return <Redirect to='/cursos' />
        }
        return (
            <Form onSubmit={this.submit}>
                <Row form>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="nombre">Nombre</Label>
                            <Input type="text" name="nombre" id="nombre" placeholder="Nombre del curso" value={this.state.nombre} onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row form>
                    <Col md={6}>
                        <FormGroup>

                            <Label for="especialidad">Especialidad</Label>
                            <Input type="text" name="especialidad" id="especialidad" placeholder="Introducir especialidad" value={this.state.especialidad} onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>

                </Row>
                <Row form>



                </Row>

                <Button color="primary">Guardar</Button>
            </Form >
        );

    }

}
