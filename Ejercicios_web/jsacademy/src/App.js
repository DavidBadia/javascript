import React, { Component } from 'react';
import './App.css';
import { Container } from 'reactstrap';

//debe estar instalado: npm install react-router-dom
import { BrowserRouter, Route } from "react-router-dom";

//deben estar definidos estos componentes, que generan el contenido para cada "página"
import Home from './Componentes/HomeComponent';
import Cursos from './Componentes/CursosComponent';
import Alumnos from './Componentes/AlumnosComponent';
import Navegador from './Componentes/Navegador';
import NuevoAlumno from './Componentes/NuevoAlumnoComponent';
import NuevoCurso from './Componentes/NuevoCursoComponent';





class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Container>
          <Navegador />
          
          <Route exact path="/" component={Home} />
          <Route path="/cursos" component={Cursos} />
          <Route path="/alumnos" component={Alumnos} />
          <Route path="/nuevoalumno" component={NuevoAlumno} />
          <Route path="/nuevocurso" component={NuevoCurso} />
        </Container>


      </BrowserRouter>

    );
  }
}

export default App;

