use vgames;
#Mostrar nombre de juego, plataforma, año y ventas globales en los top 10 de todos los tiempos.
SELECT 
    name, platform, year, Global_Sales
FROM
    videogames
ORDER BY Global_Sales DESC
LIMIT 10; 

#Mostrar los primeros 10 juegos en orden alfabético inverso por nombre.
SELECT 
    name
FROM
    videogames
ORDER BY name DESC
LIMIT 10; 

 #Mostrar el número de distintas plataformas (platform)
SELECT 
    COUNT(DISTINCT platform) AS Plataformas
FROM
    videogames;
   

SELECT 
    COUNT(DISTINCT name) AS Nombre
FROM
    videogames
WHERE
    platform LIKE 'ps%'; 
    #Número de juegos distintos publicados para todas las plataformas "playstation"

SELECT 
    COUNT(DISTINCT name)
FROM
    videogames
WHERE
    genre = 'sports' AND year <= 2000; 
    #Número de juegos publicados del género "sports" hasta el año 2000 incluido

SELECT 
    name, Global_Sales
FROM
    videogames
ORDER BY Global_Sales DESC
LIMIT 10; 
/*El juego con más unidades vendidas en un solo año (ventas globales) para el género "sports"
en todos los tiempos, y el año en que fue.*/

SELECT 
    name, year, global_sales
FROM
    videogames
WHERE
    genre = 'sports'
ORDER BY global_sales DESC
LIMIT 1; 
/*El juego con más unidades vendidas en un solo año (ventas globales) para el género "sports"
en todos los tiempos, y el año en que fue.*/

SELECT 
    name, year, global_sales
FROM
    videogames
ORDER BY global_sales DESC
LIMIT 10;
/*Los top 10 juegos con mayores ventas anuales de todos los tiempos. Mostrar juego, año y
ventas globales en orden descendiente.*/

/*Los juegos que han conseguido vender más de 25M de unidades anuales, mostrar nombre,
año y volumen de ventas, ordenados por año.*/
SELECT 
    name, year, global_sales
FROM
    videogames
WHERE
    global_sales > 25
ORDER BY year;


SELECT 
    name, genre, MAX(global_sales)
FROM
    videogames;

SELECT 
    year, platform, SUM(global_sales) AS Total
FROM
    videogames
GROUP BY year , platform
ORDER BY year , platform;

SELECT 
    year, platform, SUM(global_sales) AS Total
FROM
    videogames
WHERE
    year < 1990
GROUP BY year , platform
HAVING Total > 20
ORDER BY total DESC;

SELECT 
    name, platform, MAX(global_sales) AS total
FROM
    videogames
GROUP BY platform , name
ORDER BY platform , total DESC;

#Suma de juegos vendidos en total para la plataforma “PSP” (cantidad).7
SELECT 
    SUM(global_sales) AS Total
FROM
    videogames
WHERE
    platform ="PSP"
ORDER BY total DESC;

#Suma de juegos vendidos en europa y norte américa para la plataforma "wii".
SELECT 
    SUM(na_sales+eu_sales) AS Total
FROM
    videogames
WHERE
    platform = 'wii';

/*Suma de juegos de playstation vendidos en total en europa, norte américa y japón (por
separado)*/
SELECT 
    SUM(na_sales) AS usa,
    SUM(eu_sales) AS eu,
    SUM(jp_sales) AS jp
FROM
    videogames
WHERE
    platform LIKE 'ps%';

#Suponiendo la población actual, el "continente" con mayor índice de ventas por cápita 
SELECT 
    round(SUM(na_sales)/325.7, 2) AS usa,
    round(SUM(eu_sales)/741.3, 2) AS eu,
    round(SUM(jp_sales)/125.8, 2) AS jp
FROM
    videogames;

/*La suma de ventas en cada país/continente (eu, na, jp), solo para los géneros: racing,
sports y simulation (los 3 sumados en una sola columna)*/
SELECT 
    SUM(na_sales) AS usa,
    SUM(eu_sales) AS eu,
    SUM(jp_sales) AS jp
FROM
    videogames
WHERE
    genre IN ('racing' , 'sports', 'simulation');


/*La suma de ventas en cada continente, solo para los géneros: racing, sports y
simulation SEPARANDO LOS 3 GÉNEROS en 3 columnas (group by)*/
SELECT 
    genre,
    round(SUM(na_sales),2) AS usa,
    round(SUM(eu_sales),2) AS eu,
    round(SUM(jp_sales),2) AS jp
FROM
    videogames
WHERE
    genre IN ('racing' , 'sports', 'simulation')
GROUP BY genre;

#Suma total de ventas por plataforma en los años 2008-2010
SELECT group_concat(distinct(year)),
    platform, SUM(global_sales) AS total
FROM
    videogames    
    where year in (2008, 2009, 2010)
Group by platform;

#Suma de ventas por año para el género “Sports”
SELECT 
    year, SUM(global_sales)
FROM
    videogames
    where genre ='sports'
GROUP BY year desc;

#Suma de ventas por año para el género “Strategy”
SELECT 
    year, SUM(global_sales)
FROM
    videogames
    where genre ='strategy'
GROUP BY year desc;

#Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)
SELECT 
    name, eu_sales+na_sales+jp_Sales as total
FROM
    videogames
ORDER BY total DESC
LIMIT 10; 

#El año en que se vendieron más videojuegos de estrategia
SELECT 
    year, SUM(global_sales) AS total
FROM
    videogames
WHERE
    genre = 'strategy'
GROUP BY year
ORDER BY total DESC
LIMIT 1;

/*Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)
columnas: titulo, nombre de género*/
SELECT 
    name, genre, SUM(eu_sales + na_sales + jp_sales) as total
FROM
    videogames
GROUP BY name
ORDER BY total DESC
LIMIT 10;

/*Mostrar las 5 plataformas con mayor número de ventas en período 2010-2015
columnas: nombre de plataforma, suma de ventas total (eu+na+jp)*/
SELECT 
    platform, SUM(eu_sales + na_sales + jp_sales) AS ventas
FROM
    videogames
WHERE
    year BETWEEN 2010 AND 2015
GROUP BY platform
order by ventas desc
LIMIT 5;

/*Mostrar los 5 publishers con mayor número de ventas en período 2010-2015
columnas: nombre de publisher, suma de ventas total (eu+na+jp)*/
SELECT 
    publisher, SUM(eu_sales + na_sales + jp_sales) AS ventas
FROM
    videogames
WHERE
    year BETWEEN 2010 AND 2015
GROUP BY publisher
order by ventas desc
LIMIT 5;

#Mostrar evolución de ventas para las playstation en todos los años
SELECT 
    group_concat(distinct(platform)) as Plataforma, year as Año, SUM(eu_sales + na_sales + jp_sales) AS Total
FROM
    videogames
WHERE
    platform LIKE 'ps%' and platform not like 'psp' and platform not like 'psv'
GROUP BY  year
ORDER BY year;

#El año en que se vendieron más juegos de "pokemon" (en todas sus variantes)
SELECT 
    year, SUM(na_sales + eu_sales + jp_sales) AS total
FROM
    videogames
WHERE
    name LIKE '%pokemon%'
GROUP BY year
ORDER BY total DESC;